import * as fs from "fs";
import * as xml2js from "xml2js";
import { readFile } from "./shared_modules/files";
const GAME_CONFIG_PATH = "defaultAssets/gameConfig.json";
const XML_PATH = "defaultAssets/styles.xml";
const isRepetition = (data: Array<any>, target: any): boolean => {
    let flag = false;
    data.map((item: any) => {
        if (target.$.name === item.$.name) {
            flag = true;
        }
    });
    return flag;
}
module.exports = () => {
    return new Promise((resolve, reject) => {
        let supporLang: Array<string> = [];
        let defLang: string = "";
        readFile(GAME_CONFIG_PATH).then((data: any) => {
            const json = JSON.parse(data);
            supporLang = json.lang.support;
            defLang = json.lang.def;
            return readFile(XML_PATH);
        }).then((data: any) => {
            xml2js.parseString(data, (err, result) => {
                if (err) {
                    console.log(`warnning: ${err}`);
                    resolve();
                    return;
                }
                let json = result;
                // 多語系打包設定，檢查有無重複
                json.res.item.map((item: any) => {
                    if (item.$.name.indexOf(defLang + "/") !== -1) {
                        supporLang.map((lang: string) => {
                            let temp = JSON.parse(JSON.stringify(item));
                            temp.$.name = temp.$.name.replace(defLang + "/", lang + "/");
                            if (!isRepetition(json.res.item, temp)) {
                                json.res.item.push(temp)
                            }
                        })
                    }
                })

                // create a new builder object and then convert
                // our json back to xml.
                const builder = new xml2js.Builder({ headless: true });
                const xml = builder.buildObject(json);

                fs.writeFile(XML_PATH, xml, (err) => {
                    if (err) {
                        reject(err);
                    }
                    else {
                        console.log("successfully written our update xml to file");
                        resolve();
                    }
                })
            })
        });
    });
}
