import fs from 'fs';
import path from 'path';

import cleanFolder from "./shared_modules/files";

const { deleteDirectory } = cleanFolder;

deleteDirectory("bin/", undefined, /^(?!.*index.html)/).catch(err => console.log(err));