import * as fs from "fs";
import * as jsdom from "jsdom";
const { JSDOM } = jsdom;
const argv = process.argv.slice(2);
const INDEX_FILE = argv[0] + "/index.html";
fs.readFile(INDEX_FILE, "utf8", (err, data) => {
    if (err) {
        console.log(`read index.html error: ${err}`);
        return;
    }
    const dom = new JSDOM(data);
    if (dom) {
        dom.window.document.body.innerHTML += `
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-119700460-2"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'UA-119700460-2');
        </script>`;
        const documentElement = dom.window.document.documentElement;
        if (documentElement) {
            fs.writeFile(INDEX_FILE, documentElement.outerHTML, { encoding: "utf8" }, () => {
                console.log("add script done")
            });
        }
    }
});