import * as xlsx from "xlsx";
import * as fs from "fs";

const xlsx2json = (file: string, mainKey: string, subKey: Array<string>, output: string) => {
    let jsonString = {};
    const workbook = xlsx.readFile(file);
    const sheetNames = workbook.SheetNames;
    const worksheet = workbook.Sheets[sheetNames[0]];
    const key = subKey.join(",");
    xlsx.utils.sheet_to_json(worksheet).map((item: any) => {
        let tmp = {};
        Object.keys(item).map((val) => {
            if (key.indexOf(val) !== -1) {
                (<any>tmp)[val] = item[val];
            }
        });
        (<any>jsonString)[item[mainKey]] = tmp;
    });

    fs.writeFile(output, JSON.stringify(jsonString), 'utf8', (err) => {
        if (err) {
            throw new Error(`save ${output} fail: ${err}`);
        }
        else {
            console.log(`the ${file} convert to json finished`);
        }
    });
}

export = xlsx2json;