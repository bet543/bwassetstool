import fs from 'fs';
import path from 'path';
import { ncp } from "ncp";

const deleteFile = (dir: string, file: string, folderRegx?: RegExp, fileRegx?: RegExp) => {
    return new Promise(function (resolve, reject) {
        var filePath = path.join(dir, file);
        if (fileRegx && file.match(fileRegx) === null) {
            resolve();
            return;
        }
        fs.lstat(filePath, function (err, stats) {
            if (err) {
                return reject(err);
            }
            if (stats.isDirectory()) {
                resolve(deleteDirectory(filePath, folderRegx, fileRegx));
            } else {
                fs.unlink(filePath, function (err) {
                    if (err) {
                        return reject(err);
                    }
                    resolve();
                });
            }
        });
    });
};

const deleteDirectory = (dir: string, folderRegx?: RegExp, fileRegx?: RegExp) => {
    return new Promise(function (resolve, reject) {
        fs.access(dir, function (err) {
            if (err) {
                console.log(`${dir} no such file or directory`);
                resolve();
                return;
            }
            if (folderRegx && dir.match(folderRegx) === null) {
                resolve();
                return;
            }
            fs.readdir(dir, function (err, files) {
                if (err) {
                    return reject(err);
                }
                Promise.all(files.map(function (file) {
                    return deleteFile(dir, file, folderRegx, fileRegx);
                })).then(function () {
                    if (dir.indexOf("/") !== -1) {
                        resolve();
                    }
                    else {
                        fs.rmdir(dir, function (err) {
                            if (err) {
                                return reject(err);
                            }
                            // console.log(`Delete directory ${dir} done!!!`);
                            resolve();
                        });
                    }
                }).catch(reject);
            });
        });
    });
};

const readFile = (path: string): Promise<{}> => {
    return new Promise((resolve, reject) => {
        fs.readFile(path, "utf8", (err, jsonData) => {
            if (err) {
                console.log(`read ${path} error: ${err}`);
                reject(err);
            }
            else {
                resolve(jsonData)
            }
        });
    });
}

const copyFile = (source: string, destination: string, isOverWrite: boolean): Promise<{}> => {
    return new Promise((resolve, reject) => {
        ncp(source, destination, { clobber: isOverWrite }, (err) => {
            if (err) {
                console.log("copyFile warning: " + err);
            }
            resolve();
        });
    });
}

const getAllFilesFromDir = (startPath: string, filter: string) => {

    //console.log('Starting from dir '+startPath+'/');
    let files: Array<string> = [];
    if (!fs.existsSync(startPath)) {
        console.log(`no dir ${startPath}`);
        return [];
    }

    fs.readdirSync(startPath).map((item: string) => {
        const filename = path.join(startPath, item);
        const stat = fs.lstatSync(filename);
        if (stat.isDirectory()) {
            files.push(...getAllFilesFromDir(filename, filter)); //recurse
        }
        else if (filename.indexOf(filter) >= 0) {
            files.push(filename);
            console.log('found: ', filename);
        };
    });
    return files;
};

const copyFileSync = (source: string, target: string) => {

    let targetFile = target;

    //if target is a directory a new file with the same name will be created
    if (fs.existsSync(target)) {
        if (fs.lstatSync(target).isDirectory()) {
            targetFile = path.join(target, path.basename(source));
            if (fs.existsSync(source)) {
                fs.writeFileSync(targetFile, fs.readFileSync(source));
            }
            else {
                console.log(`not found ${source} file`);
            }
        }
    }
    else if (fs.existsSync(source)) {
        fs.writeFileSync(targetFile, fs.readFileSync(source));
    }
    else {
        console.log(`not found ${source} file`);
    }

}

const copyFolderRecursiveSync = (source: string, target: string) => {
    let files = [];

    //check if folder needs to be created or integrated
    var targetFolder = path.join(target, path.basename(source));
    if (!fs.existsSync(targetFolder)) {
        fs.mkdirSync(targetFolder);
    }

    //copy
    if (fs.lstatSync(source).isDirectory()) {
        files = fs.readdirSync(source);
        files.forEach(function (file) {
            let curSource = path.join(source, file);
            if (fs.lstatSync(curSource).isDirectory()) {
                copyFolderRecursiveSync(curSource, targetFolder);
            } else {
                copyFileSync(curSource, targetFolder);
            }
        });
    }
}

const files = {
    deleteDirectory,
    deleteFile,
    readFile,
    copyFile,
    getAllFilesFromDir,
    copyFileSync,
    copyFolderRecursiveSync,
};

export = files;