import { exec } from "child_process";

export = (cli: string) => {
    return new Promise((resolve, reject) => {
        exec(cli, (error, stdout, stderr) => {
            if (error) {
                reject(new Error(`git clone error: ${error}`));
            }
            else {
                resolve();
            }
        })
    });
}