﻿import * as fs from "fs";
import * as ncp from "ncp";
import * as xml2js from "xml2js";

const GAME_CONFIG_PATH = "laya/assets/gameConfig.json";
const XML_PATH = "laya/styles.xml";

const copyFile = (source: string, destination: string, isOverWrite: boolean): Promise<{}> => {
    return new Promise((resolve, reject) => {
        ncp.ncp(source, destination, { clobber: isOverWrite }, (err) => {
            if (err) {
                reject(err);
            }
            else {
                resolve()
            }
        });
    });
}

const isRepetition = (data: Array<any>, target: any): boolean => {
    let flag = false;
    data.map((item: any) => {
        if (target.$.name === item.$.name) {
            flag = true;
        }
    });
    return flag;
}

// 讀取 gameConfig.json 目前所支援的語系
fs.readFile(GAME_CONFIG_PATH, "utf8", (err, jsonData) => {
    if (err) {
        console.log(`read gameConfig.json error: ${err}`);
        return;
    }
    const json = JSON.parse(jsonData);
    const supporLang = json.lang.support;
    const defLang = json.lang.def;
    const copyQueue = supporLang.map((item: string) => {
        return copyFile("laya/assets/default", "laya/assets/" + item, false);
    });

    // start copy default assets
    Promise.all([copyFile("laya/assets/en-us", "laya/assets/default", true), ...copyQueue]).then(() => {
        console.log(`copy default assets done`);

        // 讀取圖檔打包設定擋
        fs.readFile(XML_PATH, (err, data) => {
            if (err) {
                console.log(`read filde error: ${err}`);
                return;
            }

            // xml to json
            xml2js.parseString(data, (err, result) => {
                if (err) {
                    console.log(`error: ${err}`);
                    return;
                }
                let json = result;
                json.res.item.map((item: any) => {
                    if (item.$.name.indexOf("default/") !== -1) {
                        supporLang.map((lang: string) => {
                            let temp = JSON.parse(JSON.stringify(item));
                            temp.$.name = temp.$.name.replace("default/", lang + "/");
                            if (!isRepetition(json.res.item, temp)) {
                                json.res.item.push(temp)
                            }
                        })
                    }
                })

                // create a new builder object and then convert
                // our json back to xml.
                const builder = new xml2js.Builder({ headless: true });
                const xml = builder.buildObject(json);

                fs.writeFile(XML_PATH, xml, (err) => {
                    if (err) console.log(err);

                    console.log("successfully written our update xml to file");
                })
            })
        })

    }).catch((err) => {
        console.error(err.message);
    });
});