/**
 * set develop asset
 */
// "Slot_RollingCars" "desktop";
import cleanFolder from "./shared_modules/files";
import copyUIEditorScript from "./copyUIEditorScript";
import fs from "fs";
const { deleteDirectory } = cleanFolder
const cloneAssets = require("./cloneAssets");
const copyDefaultAssets = require("./copyDefaultAssets");
const copyXLSX = require("./copyXLSX");
const setStyleXML = require("./setStyleXML");
const argv = process.argv.slice(2);
const prodId = argv[0];
const device = argv[1];
const assetsTag = argv[2];

deleteDirectory("laya").then(() => {
    return deleteDirectory("defaultAssets");
}).then(() => {
    return deleteDirectory("bwgame_commonassets");
}).then(() => {
    if (!fs.existsSync("laya")){
        fs.mkdirSync("laya");
    }

    if (!fs.existsSync("laya/assets")){
        fs.mkdirSync("laya/assets");
    }

    if (!fs.existsSync("laya/pages")){
        fs.mkdirSync("laya/pages");
    }
    
    // 載入Repository
    return cloneAssets(prodId, device, assetsTag);
}).then(() => {
    // 設定打包不打包
    return setStyleXML();
}).then(() => {
    // string table, share string table and sound table convert excel to json
    return copyXLSX();
}).then(() => {
    // copy default assets to other language folder
    return copyDefaultAssets(device);
}).then(() => {
    console.log("Success!!!!");
    return copyUIEditorScript();
}).catch((err: Error) => {
    console.log(err);
});