import { readFile } from "./shared_modules/files";
import child_process from "./shared_modules/process";

const cli = "copyfiles -f node_modules/bwcore_layabox/dist/libs/* bin/libs && \
            copyfiles -f node_modules/bwcore_layabox/dist/libs/min/* bin/libs/min && \
            copyfiles -f node_modules/bwslotgame/dist/BWSlotGame.d.ts libs && \
            copyfiles -f node_modules/bwslotgame/dist/BWSlotGame.js bin/libs && \
            copyfiles -f node_modules/bwcore_layabox/dist/BWGame.d.ts libs && \
            copyfiles -f node_modules/bwcore_layabox/dist/BWGame.js bin/libs && \
            copyfiles -f node_modules/bwcore_layabox/dist/LZString_1_4_3.js bin/libs && \
            copyfiles -f node_modules/bwcore_layabox/libs/Photon-Javascript_SDK.d.ts libs && \
            copyfiles -f node_modules/bwcore_layabox/libs/LayaAir.d.ts libs";


child_process(cli).then(() => {
    console.log("copy share script done");
})