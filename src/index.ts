﻿import * as process from "child_process";
import * as fs from "fs";

const GAME_CONFIG_PATH = "laya/assets/gameConfig.json";
const exec = process.exec;
const cli = "copyfiles -f node_modules/bwslotgame/dist/BWSlotGame.d.ts libs && \
            copyfiles -f node_modules/bwslotgame/dist/BWSlotGame.js bin/libs && \
            copyfiles -f node_modules/bwcore_layabox/dist/polyfill.min.js bin/libs && \
            copyfiles -f node_modules/bwcore_layabox/dist/BWGame.d.ts libs && \
            copyfiles -f node_modules/bwcore_layabox/dist/BWGame.js bin/libs && \
            copyfiles -f node_modules/bwcore_layabox/dist/Photon-Javascript_SDK.d.ts libs && \
            copyfiles -f node_modules/bwcore_layabox/dist/Photon-Javascript_SDK.js bin/libs && \
            copyfiles -f node_modules/bwgame_commonassets/StringTables/Shared_String_Dictionary.json laya/assets/StringTables && \
            copyfiles -f node_modules/bwgame_commonassets/MP3/*.mp3 laya/assets/SoundTables/MP3/share";



exec(cli, (error, stdout, stderr) => {
    if (error) {
        console.log(`\n The CLI lead to error is: ${stderr} \n`);
        return;
    }

    // 讀取 gameConfig.json 目前所支援的語系
    fs.readFile(GAME_CONFIG_PATH, "utf8", (err, jsonData) => {
        if (err) {
            console.log(`read gameConfig.json error: ${err}`);
            return;
        }
        console.log("copy share script done");
    });
});