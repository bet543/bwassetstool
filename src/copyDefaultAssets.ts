import { copyFile, readFile, copyFileSync } from "./shared_modules/files";

const DEFAULT_FOLDER = "defaultAssets"
const GAME_CONFIG_PATH = DEFAULT_FOLDER + "/gameConfig.json";
const COMMON_FOLDER = DEFAULT_FOLDER + "/common";
const BITMAPFONT_FOLDER = DEFAULT_FOLDER + "/BitmapFont";
const FONT_FOLDER = DEFAULT_FOLDER + "/font";
const ASSETS_FOLDER = "laya/assets/";
module.exports = (device: string): Promise<{}> => {
    // 讀取 gameConfig.json 目前所支援的語系
    return new Promise((resolve) => {
        readFile(GAME_CONFIG_PATH).then((jsonData: any) => {
            const json = JSON.parse(jsonData);
            const { def, support } = json.lang;
            const DEVICE_FOLDER = DEFAULT_FOLDER + "/" + device.toLowerCase();
            console.log("start copy common assets......");
            // 基本json config
            Promise.all([
                copyFileSync(DEFAULT_FOLDER + "/.laya", "laya/.laya"),
                copyFileSync(DEFAULT_FOLDER + "/styles.xml", "laya"),
                copyFileSync(DEFAULT_FOLDER + "/assets.json", ASSETS_FOLDER),
                copyFileSync(DEFAULT_FOLDER + "/gameConfig.json", ASSETS_FOLDER),
                copyFileSync(DEVICE_FOLDER + "/deviceConfig.json", ASSETS_FOLDER),
                copyFileSync(DEFAULT_FOLDER + "/modules.json", ASSETS_FOLDER)
            ]).then(() => {
                // 將各個語系複製到 laya/assets底下的各個語系
                return Promise.all(
                    [...support.map((item: string) => {
                        return copyFile(DEFAULT_FOLDER + "/" + item, ASSETS_FOLDER + item, false);
                    })]
                )
            }).then(() => {
                // 將device底下各個語系複製到 laya/assets底下的各個語系
                return Promise.all(
                    [...support.map((item: string) => {
                        return copyFile(DEVICE_FOLDER + "/" + item, ASSETS_FOLDER + item, false);
                    })]
                )
            }).then(() => {
                // 將common複製到 laya/assets底下的各個語系
                return copyFile(COMMON_FOLDER, ASSETS_FOLDER + def, false);
            }).then(() => {
                // 遭device 底下的common複製到 laya/assets底下的各個語系
                return copyFile(DEVICE_FOLDER + "/common", ASSETS_FOLDER + def, false);
            }).then(() => {
                // 將laya/assets預設的語系複製到各個語系
                return Promise.all(
                    [...support.map((item: string) => {
                        return copyFile(ASSETS_FOLDER + def, ASSETS_FOLDER + item, false);
                    })]
                )
            }).then(() => {
                return copyFile(BITMAPFONT_FOLDER, ASSETS_FOLDER + "/BitmapFont", false);
            }).then(() => {
                return copyFile(DEFAULT_FOLDER + "/images", ASSETS_FOLDER + "/images", false);
            }).then(() => {
                return copyFile(DEFAULT_FOLDER + "/pages", "laya/pages", false);
            }).then(() => {
                return copyFile(FONT_FOLDER, ASSETS_FOLDER + "/font", false);
            }).then(() => {
                // ignore share assets
                // exec("git status --porcelain | grep '^??' | cut -c4- >> .gitignore");
                console.log(`copy assets done`);
                resolve();
            })
        });
    })
}
