import xlsx2json from "./shared_modules/xlsx";
import { readFile, copyFile, getAllFilesFromDir } from "./shared_modules/files";
const GAME_CONFIG_PATH = "defaultAssets/gameConfig.json";

module.exports = () => {
    return new Promise((resolve, reject) => {
        readFile(GAME_CONFIG_PATH).then((data: any) => {
            console.log("start xlsx to json and copy string tables and sound tables......");
            const json = JSON.parse(data);
            const supporLang: string = json.lang.support.join(",");
            const soundTables = getAllFilesFromDir("defaultAssets/SoundTables/", ".xlsx");
            const stringTables = getAllFilesFromDir("defaultAssets/StringTables/", ".xlsx");
            return Promise.all([
                ...soundTables.map((filename: string) => {
                    return xlsx2json(filename, "Key", ["Filename", "isStreaming", "LoopTimes"], filename.replace(".xlsx", ".json"));
                }),
                ...stringTables.map((filename: string) => {
                    return xlsx2json(filename, "key", json.lang.support, filename.replace(".xlsx", ".json"));
                }),
                xlsx2json("bwgame_commonassets/StringTables/Shared_String_Dictionary.xlsx", "key", json.lang.support, "bwgame_commonassets/StringTables/Shared_String_Dictionary.json"),
            ]).then(() => {
                return copyFile("defaultAssets/StringTables", "laya/assets/StringTables", true);
            }).then(() => {
                return copyFile("bwgame_commonassets/StringTables", "laya/assets/StringTables", true);
            }).then(() => {
                return copyFile("defaultAssets/SoundTables", "laya/assets/SoundTables", true);
            }).then(() => {
                return copyFile("bwgame_commonassets/MP3", "laya/assets/SoundTables/MP3/share", true);
            }).then(resolve);
        })
    });
}