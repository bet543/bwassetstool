import uuid from "uuid";
import * as fs from "fs";

const argv = process.argv.slice(2);
const PATH = argv[0];
const id = uuid();

fs.readFile(PATH + "/index.html", "utf8", (err, data) => {
    fs.writeFile(PATH + "/index.html", data.replace(/\.js/g, ".js?v=" + id), { encoding: "utf8" }, () => {
        console.log("append html file query string success");
    });
})

fs.readFile(PATH + "/main.min.js", "utf8", (err, data) => {
    fs.writeFile(PATH + "/main.min.js", data.replace(/\.json/g, ".json?v=" + id), { encoding: "utf8" }, () => {
        console.log("append html file query string success");
    });
})

fs.readFile(PATH + "/assets.json", "utf8", (err, data) => {
    fs.writeFile(PATH + "/assets.json",
        data.replace(/\.json/g, ".json?v=" + id)
            .replace(/\.png/g, ".png?v=" + id)
            .replace(/\.jpg/g, ".jpg?v=" + id)
            .replace(/\.sk/g, ".sk?v=" + id)
            .replace(/\.ttf/g, ".ttf?v=" + id)
            .replace(/\.fnt/g, ".fnt?v=" + id),
        { encoding: "utf8" }, () => {
            console.log("append assets file query string success");
        });
})