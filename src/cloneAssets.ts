import { readFile } from "./shared_modules/files";
import child_process from "./shared_modules/process";

const capitalizeFirstLetter = (str: string): string => {
    return str.charAt(0).toUpperCase() + str.slice(1);
}

module.exports = (prodId: string, device: string, assetsTag?: string): Promise<{}> => {
    return readFile("package.json").then((result: any) => {
        const jsonData = JSON.parse(result);
        const dependencies = jsonData.dependencies;
        if (!dependencies.bwgame_commonassets) {
            throw new Error("bwgame_commonassets is not found");
        }
        const repository = "ssh://git@bb.allstar-interactive.com:7999/bg/bw" + prodId.toLowerCase() + "_assets.git";
        const defaultAssetsBranch = assetsTag || "develop";
        const defaultAssetsFolder = "./defaultAssets";
        const commonAssetsInfo = dependencies.bwgame_commonassets.split("#");
        const commonAssetsRepository: string = commonAssetsInfo[0];
        const commonAssetsBranch: string = commonAssetsInfo[1];
        const commonAssetsFolder = "./bwgame_commonassets";


        const cli = `git clone -b ${defaultAssetsBranch} ${repository} ${defaultAssetsFolder} && git clone -b ${commonAssetsBranch} ${commonAssetsRepository} ${commonAssetsFolder}`;
        console.log(`start git clone: ${cli}`);
        return child_process(cli);
    })
}